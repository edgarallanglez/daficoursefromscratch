//
//  UserTableCell.swift
//  DafiCourseFromScratch
//
//  Created by Edgar Allan Glez on 1/15/20.
//  Copyright © 2020 Edgar Allan Glez. All rights reserved.
//

import UIKit

class UserTableCell: UITableViewCell {
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    
}
