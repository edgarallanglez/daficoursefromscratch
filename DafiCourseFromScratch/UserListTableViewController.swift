//
//  UserListTableViewController.swift
//  DafiCourseFromScratch
//
//  Created by Edgar Allan Glez on 1/15/20.
//  Copyright © 2020 Edgar Allan Glez. All rights reserved.
//
import UIKit

class UserListTableViewController: UITableViewController {
    var userList: [[String: Any]]?
    var nombre: String?

    override func viewDidLoad() {
        let session = URLSession.shared
        let url = URL(string: "https://jsonplaceholder.typicode.com/users")!
        
        // 1st paramL URL, 2nd param completitionHandler () ->
        let task = session.dataTask(with: url) { data, response, error in

            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
               return
            }

            do {
                self.userList = (try JSONSerialization.jsonObject(with: data!, options: []) as! [[String : Any]])

                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            } catch {
               print("JSON error: \(error.localizedDescription)")
            }
       }

     task.resume()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
 
        return self.userList?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserTableCell = self.tableView.dequeueReusableCell(withIdentifier: "UserTableCell") as! UserTableCell
        
        if self.userList != nil {
            cell.nameLabel.text = self.userList?[indexPath.row]["name"] as! String
            cell.usernameLabel.text = self.userList?[indexPath.row]["username"] as! String
            cell.emailLabel.text = self.userList?[indexPath.row]["email"] as! String
            cell.phoneLabel.text = self.userList?[indexPath.row]["phone"] as! String
        }
        
        return cell
    }
    
}
