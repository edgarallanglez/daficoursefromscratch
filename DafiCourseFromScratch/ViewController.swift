//
//  ViewController.swift
//  DafiCourseFromScratch
//
//  Created by Edgar Allan Glez on 1/14/20.
//  Copyright © 2020 Edgar Allan Glez. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var triggerSegueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
       
    }
    
    
    @IBAction func triggerSegueAction(_ sender: UIButton) {
        let purpleVC = self.storyboard?.instantiateViewController(withIdentifier: "PurpleViewController") as! PurpleViewController
        
        self.navigationController?.pushViewController(purpleVC, animated: true)
    }
}

