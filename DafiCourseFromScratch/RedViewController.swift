//
//  RedViewController.swift
//  DafiCourseFromScratch
//
//  Created by Edgar Allan Glez on 1/15/20.
//  Copyright © 2020 Edgar Allan Glez. All rights reserved.
//

import UIKit

class RedViewController: UIViewController {
    
    override func viewDidLoad() {
        print("Red Going")
    }
    
    @IBAction func triggerUserList(_ sender: UIButton) {
        let userListVC = self.storyboard?.instantiateViewController(withIdentifier: "UserListTableViewController") as! UserListTableViewController
               
            self.navigationController?.pushViewController(userListVC, animated: true)
    }
    
}
